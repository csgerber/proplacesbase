package edu.uchicago.gerber.proplacesbase.presenter;

import android.util.Log;

import java.util.Date;

import edu.uchicago.gerber.proplacesbase.model.Business;
import edu.uchicago.gerber.proplacesbase.model.Place;
import edu.uchicago.gerber.proplacesbase.view.interfaces.ResultView;
import io.realm.Realm;


public class ResultsPresenter extends Presenter<ResultView> {


    public void saveClickedItem(Business business) {
        Place place = new Place(
                0,
                business.name,
                business.location.city,
                business.location.address.get(0),
                business.phone,
                business.url,
                business.image_url,
                business.categories.get(0).get(1),
                new Date().getTime()
        );

        Realm.getInstance(getView().provideContext()).beginTransaction();
        Realm.getInstance(getView().provideContext()).copyToRealm(place);
        Realm.getInstance(getView().provideContext()).commitTransaction();

    }

    @Override
    public void onStart() {

        Log.d("UURR", "onStart");
    }

    @Override
    public void onResume() {
        Log.d("UURR", "onResume");
    }

    @Override
    public void onStop() {
        Log.d("UURR", "onStop");
    }


}
