package edu.uchicago.gerber.proplacesbase.view.interfaces;

import edu.uchicago.gerber.proplacesbase.model.Place;
import io.realm.RealmResults;


public interface PlaceView extends BaseView {

    void displayPlaces(RealmResults<Place> places);
}
